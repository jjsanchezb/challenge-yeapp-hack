import { createRouter, createWebHistory } from "vue-router";

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: "/",
      name: "conference",
      component: () => import("../views/Conference.vue"),
    },
  ],
});

export default router;
