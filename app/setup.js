import firebase from '@react-native-firebase/app';
import auth from '@react-native-firebase/auth';
/* 
import database from '@react-native-firebase/database';
import storage from '@react-native-firebase/storage'; */
import {
  GoogleSignin,
} from '@react-native-community/google-signin';

GoogleSignin.configure({
    webClientId: '292495417842-mjn0cnfaenocjumj4419edbsjaha2f7o.apps.googleusercontent.com',
  });
  
const firebaseConfig = {
    apiKey: "AIzaSyB7vfkAMFbQh9KkOapCyqr5HVpl063f_uk",
    authDomain: "ecoideas-8333a.firebaseapp.com",
    projectId: "ecoideas-8333a",
    storageBucket: "ecoideas-8333a.appspot.com",
    messagingSenderId: "292495417842",
    appId: "1:292495417842:web:0f2bc220e63c790e4f0b6b",
    measurementId: "G-54JQRNGTGV"
};
if (!firebase.apps.length) {
    firebase.initializeApp(firebaseConfig);
  }
  
export default () => {
    return {auth};
}
