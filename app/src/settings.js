const HEADER_API = {
    "Content-Type": "application/json",
    "Accept": "application/json, text-plain, /",
    "X-Requested-With": "XMLHttpRequest",
}

const URL_MOVIES = 'https://api.themoviedb.org/3/discover/movie?api_key=d2f75c50a366b48f468d9a270511e992&sort_by=popularity.desc'

const URL_SERVER = 'http://192.168.178.135:8080/'

const USER={
    CREATE:'user/',
    EXERCISE:'exercise/',
    INFORMATION:'information/',
}

//Exportamos las variables
export {
    HEADER_API,
    URL_MOVIES,
    URL_SERVER,
    USER
}