import react, { useState } from 'react'
import { anonimus } from '../libs/helper'
import Http from '../libs/http'
import Storage from '../libs/storage'
import { URL_MOVIES, USER } from '../settings'
export default function useFetch() {

    const [response, setResponse] = useState({
        data: null, error: null, loading: false
    })

    const clear = () => {
        setResponse({
            data: null, error: null, loading: false
        })
    }

    const setLoading = (status = true) => {
        setResponse({
            data: null, error: null, loading: status
        })
    }



    const getData = async () => {
        try {
            setLoading()
            const res = await Http.instance.get(USER.EXERCISE, {})
            setLoading(false)
            if (res) {
                const item = res && res.length ? res[res.length - 1] : null
                setResponse({
                    data: item,
                    error: null
                })
            }

        } catch (error) {
            setLoading(false)
            console.error('getData error')
            setResponse({
                data: null,
                error: 'Lo sentimos los datos no fueron encontrados'
            })
        }
    }
    const getExercise = async () => {
        try {
            setLoading()
            const res = await Http.instance.get(USER.EXERCISE, {})
            setLoading(false)
            if (res) {
                const item = res && res.length ? res[res.length - 1] : null
                setResponse({
                    data: item,
                    error: null
                })
            }

        } catch (error) {
            setLoading(false)
            console.error('getData error')
            setResponse({
                data: null,
                error: 'Lo sentimos los datos no fueron encontrados'
            })
        }
    }

    const createUser = async (body = {}) => {
        try {
            setLoading()
            const res = await Http.instance.post(USER.CREATE, body)
            setLoading(false)
            if (res) {
                await Storage.instance.store('user', JSON.stringify(res))
                setResponse({
                    data: res,
                    error: null
                })
            } else {
                setResponse({
                    data: null,
                    error: 'Lo sentimos los datos no fueron encontrados'
                })
            }
        } catch (error) {
            setLoading(false)
            console.error('getData error')
            setResponse({
                data: null,
                error: 'Lo sentimos los datos no fueron encontrados'
            })
        }
    }
    const getUser = async (callback=anonimus) => {
        try {
            const json = await Storage.instance.get('user')
            const data = json?JSON.parse(json):null
            console.log('getUser storage', data)
            if(data){
                callback(data)
            }

        } catch (error) {
            console.error('getUser error', error)

        }
    }

    const sendData = async (body = {},callback=anonimus) => {
        try {
            setLoading()
            const res = await Http.instance.post(USER.INFORMATION, body)
            setLoading(false)
            callback()
            if (res) {
                setResponse({
                    data: res,
                    error: null
                })
                callback()
            } else {
                setResponse({
                    data: null,
                    error: 'Lo sentimos los datos no fueron enviados'
                })
            }
        } catch (error) {
            setLoading(false)
            console.error('getData error')
            setResponse({
                data: null,
                error: 'Lo sentimos los datos no fueron enviados'
            })
        }
    }



    return {
        ...response,
        getData,
        createUser,
        clear,
        getExercise,
        getUser,
        sendData
    }
}