import { StyleSheet, Text, View } from 'react-native'
import React, { useEffect } from 'react'
import { useNavigation } from '@react-navigation/native'
import { theme } from '../../res/theme'
import CustomText from '../util/CustomText'
import Louder from '../util/Louder'
import CustomButton from '../util/CustomButton'
import useFetch from '../../hooks/useFetch'

export default function MainScreen() {
    const navigation = useNavigation()
    const goToHome = () => navigation.navigate('HomeScreen')
    
    return (
        <View style={styles.container}>
            <Louder show={loading} />
            <CustomText title>Retos matemáticas</CustomText>
            <CustomButton title={'Buscar'} onPress={getData} />
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: theme.backgroundColor,
        justifyContent: 'center',
        alignItems: 'center',
        flex: 1

    }
})