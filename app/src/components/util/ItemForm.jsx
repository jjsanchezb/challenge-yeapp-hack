import React, { useState, Component } from 'react';
import {
    StyleSheet,
    View,
    TextInput as Input,
    TouchableOpacity,
    Text
} from 'react-native';
import { TextInput, HelperText, Badge } from 'react-native-paper';
import { anonimus } from '../../libs/helper';
import { theme } from '../../res/theme';
import CustomText from './CustomText';

export default class ItemForm extends Component {
    state = {
        showPassword: false,
    };

    render() {
        const {
            icon,
            input,
            meta,
            autoCapitalize = 'words',
            label = 'Email',
            keyboardType = 'email-address',
            maxLength,
            numeric = false,
            isPassword = false,
            multiLine = false,
            disabled = false,
            button = false,
            onPress = anonimus,
            numberOfLines = 10,
            noFlex = false
        } = this.props;

        return (
            <TouchableOpacity
                onPress={onPress}
                style={{

                }}
            >
                <TextInput
                    placeholderTextColor={theme.placeholderTextColor}
                    autoCapitalize={'none'}
                    keyboardType={keyboardType}
                    multiline={multiLine}
                    label={''}
                    placeholder={label}
                    numberOfLines={multiLine ? numberOfLines : 1}
                    value={input.value}
                    onChangeText={(str) => {
                        input.onChange(str);
                    }}
                    style={styles.input}
                    selectionColor={theme.primaryColor}
                    dense={true}
                    mode={'flat'}
                    secureTextEntry={isPassword ? true : false}
                    disabled={disabled}
                    maxLength={maxLength}
                    underlineColor={theme.gray}
                    font={theme.fontFamily.medium}
                    right={() => {
                        return (
                            <View
                                style={{
                                    height: 10,
                                    width: 10,
                                }}
                            />
                        );
                    }}
                    render={(props) => {
                        return <Input {...props} style={styles.input} />;
                    }}
                />
                {meta.error && meta.touched ? (
                    <View>
                        <Text style={styles.textError}>{meta.error} *</Text>
                    </View>
                ) : null}
            </TouchableOpacity>
        );
    }
}

const styles = StyleSheet.create({
    input: {
        backgroundColor: 'transparent',
        marginTop: 4,
        fontFamily: theme.fontFamily.medium,
        fontSize: theme.fontSize.input - 2,
        color: theme.green,
    },
    textError: {
        color: theme.colors.danger,
        fontFamily: theme.fontFamily.medium,
        fontSize: theme.fontSize.input,
        alignSelf:'flex-end'
    },
});
