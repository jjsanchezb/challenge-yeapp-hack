// import React from 'react'
// import { StyleSheet, Text, View } from 'react-native'
// import DocumentPicker from 'react-native-document-picker';
// import { theme } from '../../res/theme';
// import { HelperText } from 'react-native-paper';

// export default function UploadPdf({
//   input,
//   meta,
//   label,
//   button = false,
//   type,
//   stylebtn = {},
//   typeFile=false
// }) {
//   const { data, loading, error, uploadFiles } = useUploadFiles();

//   const docPicker = async () => {
//     try {
//       const res = await DocumentPicker.pick({
//         type: typeFile?[DocumentPicker.types.images,DocumentPicker.types.pdf]:[DocumentPicker.types.pdf],
//       });

//       const res_data = { "name": res.name, "type": res.type, "uri": res.uri }

//       const fileFormData = new FormData();
//       fileFormData.append('files', res_data);

//       uploadFiles(fileFormData, true, (responsedata) => {
//         try {
//           const response = responsedata.files
//           const split = response ? response.url.origin.split('/') : ''
//           const nombrePDF = split[split.length - 1]
//           input.onChange({ name: nombrePDF, url: response.url.origin, type: response.type });
//         } catch (error) {
//           console.log('error docPicker response', error);

//         }
//       });
//     } catch (err) {
//       console.log('error docPicker', err);

//       if (DocumentPicker.isCancel(err)) {
//         input.onChange({})
//         console.log('error -----', err);
//       } else {
//         input.onChange({})
//         throw err;
//       }
//     }
//   };

//   return (
//     <View>
//       <Button title={label}
//         onPress={docPicker}
//         loading={loading}
//         style={stylebtn}
//       >
//         {meta.error && meta.touched ? (
//           <HelperText>
//             <Text style={styles.textError}>{meta.error} *</Text>
//           </HelperText>
//         ) : null}
//       </Button>
//       <Text style={styles.fileName}>{input.value?.name}</Text>

//     </View>
//   )
// }

// const styles = StyleSheet.create({
//   fileName: {
//     justifyContent: 'center',
//     textAlign: 'center',
//     fontFamily: 'Montserrat-Regular',
//   },
//   textError: {
//     color: theme.DANGER_COLOR,
//     fontFamily: theme.fontFamily.medium,
//     fontSize: theme.fontSize.input,
//   },
// })
