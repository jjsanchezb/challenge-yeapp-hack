import { StyleSheet, Text, View } from 'react-native'
import React from 'react'
import { theme } from '../../res/theme';

export default function CustomText({ children, title, subtitle, description }) {
    if (title) {
        return (
            <Text style={styles.title}>{children}</Text>

        )
    } else if (subtitle) {
        return (
            <Text style={styles.subtitle}>{children}</Text>

        )
    } else if (description) {
        return (
            <Text style={styles.description}>{children}</Text>

        )
    } else {
        return (
            <Text style={styles.description}>{children}</Text>

        )
    }

}

const styles = StyleSheet.create({
    title: {
        color: 'black',
        fontFamily: theme.fontFamily.bold,
        textAlign: 'center',
        fontSize: 20
    },
    subtitle: {
        color: 'black',
        fontFamily: theme.fontFamily.medium,
        textAlign: 'center',
        fontSize: 18
    },
    description: {
        color: 'black',
        fontFamily: theme.fontFamily.regular,
        textAlign: 'center',
        fontSize: 15
    },
})