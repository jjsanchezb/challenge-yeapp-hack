import { StyleSheet, Text, View } from 'react-native'
import React from 'react'
import { ActivityIndicator } from 'react-native-paper'

export default function Loder() {
    return (
        <Center>
            <ActivityIndicator size={40} />
        </Center>
    )
}

const styles = StyleSheet.create({})