import { StyleSheet, Text, View, TouchableOpacity } from 'react-native'
import React, { useState } from 'react'
import { theme } from '../../res/theme';
import { TextInput } from 'react-native-paper'
import Icon from 'react-native-vector-icons/MaterialCommunityIcons'
export default function Item({ num = 0, label = '', onPress = () => { }, simple = false, extra, input,correct }) {
    const [visible, setVisible] = useState(false)
    const [value, setValue] = useState('')
    return (
        <View>


            <TouchableOpacity style={{
                flexDirection: 'row',
                backgroundColor: 'white',
                borderRadius: 100,
                alignItems: 'center',
                paddingRight: 80,
                borderWidth: 2,
                borderColor: '#FF5722',
                marginVertical: 10
            }}
                onPress={() => {
                    if(visible){
                        setValue('')
                    }
                    setVisible(!visible)
                    if (!input) {
                        onPress()
                    }
                }}
            >
                <View style={{
                    backgroundColor: '#FF5722',
                    width: 50,
                    height: 50,
                    borderRadius: 100,
                    justifyContent: 'center',
                    alignItems: 'center',
                    marginRight: 10
                }}>

                    <Text style={styles.title}>{num}</Text>
                </View>
                <Text style={styles.medium}>{label}</Text>

            </TouchableOpacity>
            {
                visible && simple ?
                    <View style={{
                        backgroundColor: '#d3d3d3',
                        borderRadius: 12,
                        padding: 10
                    }}>
                        <Text style={styles.medium}>{extra}</Text>
                    </View> : null
            }
            {
                visible && input ?
                    <View style={{
                        backgroundColor: '#d3d3d3',
                        borderRadius: 12,
                        padding: 10,
                        flexDirection: 'row',
                        alignItems:'center'
                    }}>
                        <TextInput
                            style={{
                                backgroundColor: 'white',
                                flex: 1
                            }}
                            value={value}
                            onChangeText={str => setValue(str?.toLowerCase())}

                        />
                        <View style={{
                            backgroundColor: '#4CAF50',
                            borderRadius: 120,
                            width: 50,
                            height: 50,
                            justifyContent: 'center',
                            alignItems: 'center',
                            marginLeft:10
                        }}>
                            <Icon name='check' size={30} onPress={()=>{
                                if(correct?.includes(value)){
                                    onPress()
                                }else{
                                    alert('La respuesta no es correcta')
                                }
                            }} />
                        </View>
                    </View> : null
            }
        </View>
    )
}

const styles = StyleSheet.create({
    title: {
        color: 'black',
        fontFamily: theme.fontFamily.bold,
        fontSize: 20

    },
    medium: {
        color: 'black',
        fontFamily: theme.fontFamily.medium,
        fontSize: 15

    },
})