import { StyleSheet, Text, View } from 'react-native'
import React from 'react'
import { Button } from 'react-native-paper'
import { theme } from '../../res/theme'

export default function CustomButton({ title, onPress = () => {

}, primary = true }) {
    return (
        <Button mode={'contained'} onPress={onPress} style={[styles.button, {
            backgroundColor: primary ? theme.accentColor : theme.primaryColor
        }]}
        >
            <Text style={styles.title}>{title}</Text>
        </Button>
    )
}

const styles = StyleSheet.create({
    button: {
        width: '80%',
        borderRadius: 100,
        paddingVertical: 5
    },
    title: {
        color: 'white',
        fontFamily: theme.fontFamily.bold,
        textAlign: 'center',
        fontSize: 14,
        
    },
})