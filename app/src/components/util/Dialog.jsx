import * as React from 'react';
import { View, StyleSheet } from 'react-native';
import {
    Button,
    Paragraph,
    Dialog,
    Portal,
    Modal,
} from 'react-native-paper';
import { anonimus } from '../../libs/helper';
import { theme } from '../../res/theme';

export default class MyDialog extends React.Component {
    state = {
        visible: false,
        title: '',
        description: '',
        onPress: anonimus,
    };
    open = (
        title,
        description,
        onPress = anonimus,
    ) => {
        this.setState({
            title: title,
            description: description,
            onPress,
            visible: true,

        });
    };
    close = () =>
        this.setState({
            visible: false,
            title: '',
            description: '',
            onPress: anonimus,
        });

    render() {
        const {
            visible,
            title,
            description,
            onPress,
        } = this.state;
        const { dismissable = true } = this.props;
        return (
            <View>
                <Portal>
                    <Dialog
                        visible={visible}
                        onDismiss={this.close}
                        dismissable={dismissable}>
                        <Dialog.Title style={styles.title}>{title}</Dialog.Title>
                        <Dialog.Content>
                            <Paragraph style={styles.description}>{description}</Paragraph>

                        </Dialog.Content>
                        <Dialog.Actions>
                            <Button
                                onPress={() => {
                                    this.close();
                                }}>
                                Cancelar
                            </Button>

                            <Button
                                onPress={() => {
                                    this.close();
                                    onPress();
                                }}>
                                OK
                            </Button>
                        </Dialog.Actions>
                    </Dialog>
                </Portal>
            </View>
        );

    }
}

const styles = StyleSheet.create({
    title: {
        color: '#212121',
    },
    description: {
        color: '#757575',
    },
});
