import { StyleSheet, Text, View } from 'react-native'
import React from 'react'

export default function Center({ children }) {
    return (
        <View style={styles.center}>
            {children}
        </View>
    )
}

const styles = StyleSheet.create({
    center: {
        justifyContent: 'center',
        alignItems: 'center'
    }
})