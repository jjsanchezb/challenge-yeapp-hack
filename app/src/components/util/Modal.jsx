import { Text, View, Dimensions, StyleSheet } from 'react-native'
import React, { Component } from 'react'
import { Modal, Portal, Button, Provider } from 'react-native-paper';
import LottieView from 'lottie-react-native';
const { width, height } = Dimensions.get('screen')
import { theme } from '../../res/theme';
import { anonimus } from '../../libs/helper';

export default class MyModal extends Component {
    state = {
        visible: false,
        type: '',
        text: '',
        points: 0,
        onPress:anonimus
    }


    open = (type, text, points = 0,onPress=anonimus) => this.setState({ visible: true, type, text, points,onPress });
    showModal = () => {
        this.setState({ visible: true });
    }
    hideModal = () => {
        this.setState({ visible: false,...this.state });
        this.state.onPress()
    }
    render() {
        const { visible, type, text, points } = this.state
        let source = source = require('../../assets/img/success1.json')
        if (type == 'success1') {
            source = require('../../assets/img/success1.json')
        } else if (type == 'success2') {
            source = require('../../assets/img/success2.json')

        } else if (type == 'error') {
            source = require('../../assets/img/error.json')
        }
        return (
            <Provider>
                <Portal>
                    <Modal visible={visible} onDismiss={this.hideModal} contentContainerStyle={{
                        marginHorizontal: 20,
                        backgroundColor: 'white',
                        minHeight: 400,
                        borderRadius: 40,

                    }}>
                        <View style={{
                            justifyContent: 'center',
                            alignItems: 'center',
                            flex: 1
                        }}>

                            <View>
                                <LottieView source={source}
                                    autoPlay
                                    loop
                                    style={{
                                        backgroundColor: 'white',
                                        width: width / 1.8,
                                        height: width / 1.8,
                                        alignSelf: 'center'

                                    }} />
                                <View style={styles.content}>
                                    <Text style={styles.points}>Ganaste {points}</Text>
                                    <Text style={styles.subtitle}>puntos</Text>
                                    <Text style={styles.medium}>{text}</Text>
                                </View>

                            </View>
                        </View>
                        <Button mode='contained' style={{
                            marginHorizontal: 10,
                            borderRadius: 100,
                            backgroundColor: '#757575',
                            marginVertical: 10
                        }}
                            onPress={this.hideModal}
                        >Cerrar</Button>

                    </Modal>
                </Portal>

            </Provider>
        )
    }
}

const styles = StyleSheet.create({
    points: {
        color: '#FFC107',
        fontFamily: theme.fontFamily.bold,
        fontSize: 30,
        textAlign: 'center'

    },
    subtitle: {
        color: '#FFC107',
        fontFamily: theme.fontFamily.medium,
        fontSize: 20,
        textAlign: 'center',
        marginTop: -10

    },

    title: {
        color: 'black',
        fontFamily: theme.fontFamily.bold,
        fontSize: 20

    },
    medium: {
        color: 'black',
        fontFamily: theme.fontFamily.medium,
        fontSize: 15,
        textAlign: 'center'

    },
    content: {
        marginHorizontal: 10
    }
})