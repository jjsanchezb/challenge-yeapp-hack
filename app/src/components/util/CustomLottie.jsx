import { StyleSheet, Text, View, Dimensions } from 'react-native'
import React from 'react'
import LottieView from 'lottie-react-native';
const { width } = Dimensions.get('screen')

export default function CustomLottie({
    dimension = 1,
    source = require('../../assets/img/money.json'),
    style={}

}) {
    return (
        <LottieView 
            source={source}
            autoPlay
            loop
            style={{
                backgroundColor: 'white',
                width: width / dimension,
                height: width / dimension,
                alignSelf: 'center',
                ...style
            }} />
    )
}

const styles = StyleSheet.create({})