import { StyleSheet, Text, View } from 'react-native'
import React from 'react'
import { ActivityIndicator } from 'react-native-paper'
import Center from './Center'

export default function Loder() {
    return (
        <Center>
            <ActivityIndicator size={40} />
        </Center>
    )
}

const styles = StyleSheet.create({})
// import { StyleSheet, Text, View } from 'react-native'
// import React from 'react'
// import CustomLottie from './CustomLottie'

// export default function Louder({show=false}) {
//     if(show){
//         return (
//             <CustomLottie source={require('../../assets/img/louder5.json')} dimension={2} />
//         )
//     }else{
//         return null
//     }
  
// }

// const styles = StyleSheet.create({})