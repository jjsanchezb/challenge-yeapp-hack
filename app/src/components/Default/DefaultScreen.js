import React from 'react'
import { StyleSheet, Text, View } from 'react-native'
import CustomText from '../util/CustomText'
import CustomButton from '../util/CustomButton'
import { useNavigation } from '@react-navigation/native'
import SizeBox from '../util/SizeBox'

export default function DefaultScreen(props) {
    const navigation = useNavigation()
    const { params } = props.route

    return (
        <View style={{
            flex: 1,
            justifyContent: 'center',
            alignItems: 'center'
        }}>
            <CustomText title>Presentar otro ejercicio</CustomText>
            <SizeBox height={30}/>
            <CustomButton
                primary={false}
                title={'Buscar'}
                onPress={() => {
                    navigation.navigate('HomeScreen', params)
                }}
            />

        </View>
    )
}

const styles = StyleSheet.create({})