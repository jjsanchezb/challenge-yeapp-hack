import { StyleSheet, Text, View, Dimensions } from 'react-native'
import React from 'react'
import CustomLottie from '../util/CustomLottie';
import CustomText from '../util/CustomText';
import SizeBox from '../util/SizeBox';
const { width, height } = Dimensions.get('screen')
import Item from '../util/Item';
import { anonimus } from '../../libs/helper';

export default function Screen4({ open = anonimus }) {
    return (
        <View style={[{
            backgroundColor: 'white',
            marginHorizontal: 0,
            flex: 1,
            paddingTop: 100,
            paddingHorizontal: 20
        }]}>

            <CustomText title>¿Cómo lo resolverias?</CustomText>
            <SizeBox height={20} />
            <Item
                num={1}
                label={'Agrupar de a 5 computadores'}
                onPress={() => {
                    open('success1', 'Bien hecho, estas a un paso de alcanzar un nuevo nivel', 1000)
                }}
            />
            <Item
                num={2}
                label={'Construir la ecuación'}
                input
                onPress={() => {
                    open('success2', 'Excelente eres todo un crack!', 35000)
                }}
                correct={['3x+2x=60', '2x+3x=60']}

            />
            {/* <Item
                num={3}
                label={'Sumar de a 3 y 2 computadores hasta llegar a 60'}
                extra={'No esta mal, pero puedes hacerlo mejor'}
                simple

            /> */}
            <Item
                num={3}
                label={'La respuesta es 60.000'}
                onPress={() => {
                    open('error', 'La respuesta es incorrecta', 0)
                }}
            />




        </View>
    )
}

const styles = StyleSheet.create({
    slide: {
        height: height - 200,
        width: width - 20,
        backgroundColor: 'white',
        marginLeft: 30,
        borderRadius: 20,
        padding: 20,
        marginTop: 60,
        justifyContent: 'center',
        alignItems: 'center'

    }
})