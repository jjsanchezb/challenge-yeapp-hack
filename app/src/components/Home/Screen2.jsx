import { StyleSheet, Text, View, Dimensions } from 'react-native'
import React from 'react'
import CustomLottie from '../util/CustomLottie';
import CustomText from '../util/CustomText';
import SizeBox from '../util/SizeBox';
const { width, height } = Dimensions.get('screen')

export default function Screen2() {
    return (
        <View style={styles.slide}>
            <CustomText title>
                Durante el mes de diciembre uno de sus venderores vendió 60 computadores
            </CustomText>
            <SizeBox height={20} />
            <CustomText subtitle>
                Por cada 3 computadores de la marca 1
            </CustomText>
            <SizeBox height={20} />
            <View style={styles.row}>
                {
                    [1, 2, 3].map(item => {
                        return (
                            <CustomLottie
                                source={require('../../assets/img/pc.json')}
                                dimension={4} />
                        )
                    })
                }
            </View>
            <SizeBox height={20} />
            <CustomText subtitle>
                Vendió 2 de la marca 2
            </CustomText>
            <SizeBox height={20} />
            <View style={styles.row}>
                {
                    [1, 2].map(item => {
                        return (
                            <CustomLottie source={require('../../assets/img/pc2.json')}
                                dimension={5} 
                                style={{marginRight:10}}
                                />
                        )
                    })
                }
            </View>

        </View>
    )
}

const styles = StyleSheet.create({
    slide: {
        height: height - 200,
        width: width - 20,
        backgroundColor: 'white',
        marginLeft: 30,
        borderRadius: 20,
        padding: 20,
        marginTop: 60,
        justifyContent: 'center',
        alignItems: 'center'

    },
    row: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        flex:1
    }
})