import { StyleSheet, Text, View, Dimensions } from 'react-native'
import React from 'react'
import CustomLottie from '../util/CustomLottie';
import CustomText from '../util/CustomText';
const { width, height } = Dimensions.get('screen')

export default function Screen1() {
    return (
        <View style={styles.slide}>
            <CustomText title>
                Resuelve el siguiente ejercicio
            </CustomText>
            <CustomLottie
                dimension={1.8}
                source={require('../../assets/img/pc.json')}
            />
            <CustomLottie
                dimension={3}
                source={require('../../assets/img/pc2.json')}
            />
            <CustomText title>
                Estás en un almacén que distribuye computadores de dos marcas 1 y 2
            </CustomText>
        </View>
    )
}

const styles = StyleSheet.create({
    slide: {
        height: height - 200,
        width: width - 20,
        backgroundColor: 'white',
        marginLeft: 30,
        borderRadius: 20,
        padding: 20,
        marginTop: 60,
        justifyContent: 'center',
        alignItems: 'center'

    }
})