import { StyleSheet, Text, View, Dimensions } from 'react-native'
import React from 'react'
import CustomLottie from '../util/CustomLottie';
import CustomText from '../util/CustomText';
import SizeBox from '../util/SizeBox';
const { width, height } = Dimensions.get('screen')

export default function Screen3() {
    return (

        <View style={styles.slide}>
            <SizeBox height={20} />
            <CustomText subtitle>Si recibiste una comisión de</CustomText>
            <CustomText subtitle>$ 10.000 por la marca 1</CustomText>
            <SizeBox height={20} />
            <View style={styles.row}>
                <CustomLottie
                    source={require('../../assets/img/money.json')}
                    dimension={2} />
            </View>
            <SizeBox height={20} />
            <CustomText subtitle> y una comisión de $ 20.000 por la marca 2</CustomText>
            <SizeBox height={20} />
            <View style={styles.row}>
                <CustomLottie
                    source={require('../../assets/img/dolar.json')}
                    dimension={2} />

            </View>
            <SizeBox height={20} />
            <CustomText subtitle>¿Cuanto dinero recibiste en total?</CustomText>
        </View>

    )
}

const styles = StyleSheet.create({
    slide: {
        height: height - 200,
        width: width - 20,
        backgroundColor: 'white',
        marginLeft: 30,
        borderRadius: 20,
        padding: 20,
        marginTop: 60,
        justifyContent: 'center',
        alignItems: 'center'

    }
})