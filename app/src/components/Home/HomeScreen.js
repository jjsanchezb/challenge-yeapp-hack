import { StyleSheet, Text, View, Dimensions } from 'react-native'
import React, { useEffect, useRef } from 'react'
import Carousel from 'react-native-snap-carousel';
const { width, height } = Dimensions.get('screen')
import MyModal from '../util/Modal';
import Screen1 from './Screen1';
import Screen2 from './Screen2';
import Screen4 from './Screen4';
import Screen3 from './Screen3';
import useFetch from '../../hooks/useFetch';
import Louder from '../util/Louder';
import { useNavigation } from '@react-navigation/native';

export default function HomeScreen(props) {
    const { data, loading, error, getExercise, sendData } = useFetch()
    const navigation = useNavigation()
    const { params } = props.route
    console.log('route user', params)
    const user = params
    const ref = useRef()
    useEffect(() => {
        getExercise()
    }, [])


    const open = (type, text, points) => {
        ref.current?.open(type, text, points, () => {
            sendData({
                studentId: user?.id,
                exerciseId: data?.id || 1,
                data: {
                    correct: type == 'error' ? false : true,
                    points: points,
                    url: ''
                }
            }, () => {
                navigation.navigate('DefaultScreen')
            })
        })
        //enviar respuesta

    }


    const _renderItem = ({ item, index }) => {
        if (index == 0) {
            return (
                <Screen1 open={open}
                />
            );
        } else if (index == 1) {
            return (
                <Screen2 open={open}
                />
            );
        } else if (index == 2) {
            return (
                <Screen3 open={open}
                />
            );
        } else if (index == 3) {
            return (
                <Screen4
                    open={open}
                />
            );
        } else {
            return null
        }
    }

    if (loading) {
        return (<View style={{ flex: 1, alignItems: 'center', justifyContent: 'center', backgroundColor: 'white' }}>
            <Louder />
        </View>)
    }
    console.log('data', data)
    // if (!data) {
    //     return (<View style={{ flex: 1, alignItems: 'center', justifyContent: 'center', backgroundColor: 'white' }}>
    //         <CustomText title>
    //             No se encontraron ejercicios para realizar
    //         </CustomText>
    //     </View>)
    // }
    return (
        <View style={styles.container}>
            <Carousel
                data={[1, 2, 3, 4]}
                renderItem={_renderItem}
                sliderWidth={width}
                itemWidth={width}
            />
            <MyModal ref={ref} />
        </View>
    )
}

const styles = StyleSheet.create({
    row: {
        flexDirection: 'row',
        justifyContent: 'space-around'
    },
    container: {
        flex: 1,
        backgroundColor: 'black'
    },
    slide: {
        height: height - 200,
        width: width - 20,
        backgroundColor: 'white',
        marginLeft: 30,
        borderRadius: 20,
        padding: 20,
        marginTop: 60,
        justifyContent: 'center',
        alignItems: 'center'

    }
})