import { StyleSheet, Text, View, Image } from 'react-native'
import React, { useRef, useEffect } from 'react'
import { Form, Field } from 'react-final-form';
import CustomButton from '../util/CustomButton';
import Center from '../util/Center';
import ItemForm from '../util/ItemForm';
import SizeBox from '../util/SizeBox';
import { theme } from '../../res/theme';
import { composeValidators, required } from '../../libs/validators'
import MyDialog from '../util/Dialog';
import useFetch from '../../hooks/useFetch';
import { useNavigation } from '@react-navigation/native';
export default function RegisterScreen() {
    const ref = useRef()
    const navigation = useNavigation()
    const { data, error, createUser, clear, getUser } = useFetch()
    useEffect(() => {
        if (error) {
            ref?.current?.open('Lo sentimos', error)
        }

    }, [error])
    useEffect(() => {
        if (data) {
            ref?.current?.open('Bien hecho!', 'El estudiante ha sido creado exitosamente', () => {
                navigation.navigate('HomeScreen', data)
            })
        }

    }, [data])
    useEffect(() => {
        clear()
        //verificar si el usuario existe
        getUser((item) => {
            navigation.navigate('HomeScreen', item)
        })

    }, [])
    return (
        <View style={styles.container}>
            <View style={styles.form}>
                <Form
                    initialValues={{
                        // first_name: 'Victor',
                        // last_name: 'Murillo',
                        // username: 'mcvictor2',
                        // email: 'mcvictor2@unicauca.edu.co',
                        // password: 'secret'
                    }}
                    onSubmit={(values) => {

                        console.log('values', values)
                        createUser(values)
                        // ref?.current?.open('TItulo', 'descripcion')
                    }}>
                    {({ handleSubmit, values }) => (
                        <>

                            <Center>
                                <SizeBox height={20} />
                                <Image
                                    source={{ uri: 'https://images.unsplash.com/photo-1498050108023-c5249f4df085?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=2372&q=80' }}
                                    style={styles.img}
                                />
                            </Center>
                            <Field name="first_name" validate={composeValidators(required)}>
                                {({ input, meta }) => (
                                    <ItemForm
                                        label={'Nombre'}
                                        input={input} meta={meta} />
                                )}
                            </Field>
                            <Field name="last_name" validate={composeValidators(required)}>
                                {({ input, meta }) => (
                                    <ItemForm
                                        label={'Apellido'}
                                        input={input} meta={meta} />
                                )}
                            </Field>

                            <Field name="username" validate={composeValidators(required)}>
                                {({ input, meta }) => (
                                    <ItemForm
                                        label={'Usuario'}
                                        input={input} meta={meta} />
                                )}
                            </Field>
                            <Field name="password" validate={composeValidators(required)}>
                                {({ input, meta }) => (
                                    <ItemForm
                                        label={'Usuario'}
                                        input={input} meta={meta} />
                                )}
                            </Field>



                            <Center>
                                <SizeBox height={20} />
                                <CustomButton
                                    primary={false}
                                    title={'Crear usuario'}
                                    onPress={handleSubmit}
                                // loading={loading}
                                />
                            </Center>
                        </>
                    )}
                </Form>
            </View>
            <MyDialog ref={ref} />
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        marginHorizontal: 20,
        justifyContent: 'center'
    },
    img: {
        height: theme.dimensions.sizeImg,
        width: theme.dimensions.sizeImg,
        borderRadius: theme.dimensions.sizeImg,
        backgroundColor: theme.primaryColor,
        borderWidth: 2,
        borderColor: theme.primaryColor
    }

})