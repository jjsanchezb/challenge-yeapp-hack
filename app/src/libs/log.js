const _DEV_ = true;
export default class Log {
  static instance = new Log();

  success = (value, str = '') => {
    if (_DEV_) {
      console.log(`%c Sucsess ${str}`, colors.success, value);
    }
  };
  warning = (value, str = '') => {
    if (_DEV_) {
      console.log(`%c Warning ${str}`, colors.warning, value);
    }
  };
  info = (value, str = '') => {
    
    if (_DEV_) {
      console.log(`%c info ${str}`, colors.info, value);
    }
  };
  url = (value, str = '', body) => {
    if (_DEV_) {
      console.log(`%c url ${str}`, colors.info, value,body);
    }
  };
  post = (value, body, str = '') => {
    if (_DEV_) {
      console.log(`%c post ${str}`, colors.info, value,body);
    }
  };
  get = (value, body, str = '') => {
    if (_DEV_) {
      console.log(`%c get ${str}`, colors.info, value,body);
    }
  };
  urlError = (value, body, str = '') => {
    if (_DEV_) {
      console.log(`%c ${str}`, colors.error, value,body);
    }
  };
  token = (value, str = '') => {
    if (_DEV_) {
      console.log(`%c token ${str}`, colors.info, value);
    }
  };
  response = (value, str = '') => {
    if (_DEV_) {
      console.log(`%c response ${str}`, colors.success, value);
    }
  };
  error = (value, str = '') => {
    if (_DEV_) {
      console.log(`%c Error ${str}`, colors.error, value);
    }
  };
}

const style =
  'display: inline-block ; color: #ffffff ; font-weight: bold ; padding: 3px 7px 3px 7px ; border-radius: 3px 3px 3px 3px ;';
const colors = {
  success: `${style} background-color: #4CAF50 ;`,
  error: `${style} background-color: #F44336 ;`,
  warning: `${style} background-color: #FFC107 ;`,
  info: `${style} background-color: #2196F3 ;`,
};