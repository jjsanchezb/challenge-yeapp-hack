import {
    HEADER_API,
    URL_SERVER,
} from '../settings';
import log from './log';


export default class Http {
    static instance = new Http();

    post = async (url, body = {}) => {
        try {
            const URL = `${URL_SERVER}${url}`;
            log.instance.post(URL, body, url);
            const req = await fetch(URL, {
                headers: HEADER_API,
                method: 'POST',
                body: JSON.stringify(body),
            });
            const json = await req.json();
            log.instance.response(json, url);
            return json;
        } catch (error) {
            log.instance.urlError(error, body, url);
            return null;
        }
    };
    get = async (url, body = {}) => {
        try {
            log.instance.info('URL', url);
            const URL = `${URL_SERVER}${url}`;

            const req = await fetch(URL, {
                // headers: HEADER_API,
                method: 'GET',

            });
            const json = await req.json();
            log.instance.response(json, url);
            return json;
        } catch (error) {
            log.instance.urlError(error, body, url);
            return null;
        }
    };
}
