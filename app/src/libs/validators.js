export const required = value => (value ? undefined : 'Campo requerido');
export const required2 = value => (value ? undefined : '*');

export const mustBeNumber = value =>
  isNaN(value) ? 'Debe ser número' : undefined;

export const isEqual = str1 => str2 =>
  str1 === str2 ? undefined : `Las contraseñas deben ser iguales`;
export const minValue = min => value =>
  isNaN(value) || value >= min ? undefined : `debe ser mayor a ${min}`;
export const maxValue = max => value =>
  isNaN(value) || value <= max ? undefined : `debe ser menor a ${max}`;

export const maxLength = max => value =>
value && ((value.length) <= max) ? undefined : `No puedes ingresar mas de ${max} caracteres`;

export const minLength = min => value =>
  value && value.length > min
    ? undefined
    : `Debe ser mayor a ${min} caracteres`;

export const isEmail = value =>
  value && !/^[A-Z0-9._%+-]+@[A-Z0_9.-]+\.[A-Z]{2,4}$/i.test(value)
    ? 'Correo invalido'
    : undefined;

export const composeValidators =
  (...validators) =>
  value =>
    validators.reduce(
      (error, validator) => error || validator(value),
      undefined,
    );
