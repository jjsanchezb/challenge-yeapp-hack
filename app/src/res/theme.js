const FONT_FAMILY = 'Montserrat';

const COLORS = {
  danger: 'rgb(218,56,73)',
  success: 'rgb(48,166,74)',
  warning: 'rgb(254,192,47)',
  info: 'rgb(37,162,183)',
  light: 'rgb(248,249,250)',
  dark: 'rgb(52,58,64)',
  primary: 'rgb(21,127,251)',
  secondary: 'rgb(109,117,125)',
};

const ICON_SIZE = {
  large: 30,
  medium: 26,
  small: 22,
  default: 20,
  extraLarge: 40,
};

const FONT_SIZE = {
  extraTitle: 22,
  title: 19,
  subtitle: 16,
  description: 14,
  small: 12,
  label: 10,
  input: 12,
  btnTitle: 13,
};

const FONT_FAMILY_LIST = {
  bold: `${FONT_FAMILY}-Bold`,
  medium: `${FONT_FAMILY}-Medium`,
  regular: `${FONT_FAMILY}-Regular`,
  light: `${FONT_FAMILY}-Light`,
};

const DIMENSIONS = {
  paddingHorizontal: 20,
  sizeImg:140
};


const theme = {
  progressHeight: 2.5,
  progress: '#E64A19',
  bottomNavigation: '#FF5722',
  backgroundColor: '#ffffff',
  primaryColor: '#FF9800',
  secondaryColor: '#7C4DFF',
  darkPrimaryColor: '#F57C00',
  accentColor: '#7C4DFF',
  //texto
  primaryTextColor: '#212121',
  secondaryTextColor: '#757575',
  fontSize: FONT_SIZE,
  fontFamily: FONT_FAMILY_LIST,
  iconSize: ICON_SIZE,
  colors: COLORS,
  dimensions: DIMENSIONS,
};

const themeDark = {
  ...theme,
  backgroundColor: 'black',
  color: 'white',
  primaryColor: '#009688',
  darkPrimaryColor: '#00796B',
  accentColor: '#FF5722',
  rippleColor: 'rgba(118,111,109,.25)',
  //texto
  primaryTextColor: 'rgb(43,33,30)',
};

const getObjectFont = (property) => ({fontFamily: theme.fontFamily[property]});

const configFonts = {
  bold: getObjectFont('bold'),
  medium: getObjectFont('medium'),
  regular: getObjectFont('regular'),
  light: getObjectFont('light'),
  thin: getObjectFont('thin'),
};

const fonts = {
  ios: configFonts,
  android: configFonts,
};

const themes = [themeDark, theme];

export {theme, themes, fonts};
