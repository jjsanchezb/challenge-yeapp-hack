import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import { StyleSheet } from 'react-native';
// screens
import HomeScreen from '../components/Home/HomeScreen';
import MainScreen from '../components/Main/MainScreen';
import RegisterScreen from '../components/Register/RegisterScreen';
import DefaultScreen from '../components/Default/DefaultScreen';

const Stack = createStackNavigator();

export default function RouterStack(props) {
    return (
        <Stack.Navigator
            screenOptions={{
                headerShown: false,
            }}
            initialRouteName={'RegisterScreen'}
            >
            <Stack.Screen name="MainScreen" component={MainScreen} />
            <Stack.Screen name="HomeScreen" component={HomeScreen} />
            <Stack.Screen name="RegisterScreen" component={RegisterScreen} />
            <Stack.Screen name="DefaultScreen" component={DefaultScreen} />
        </Stack.Navigator>
    );
}

const styles = StyleSheet.create({});
