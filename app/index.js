/**
 * @format
 */

import {AppRegistry} from 'react-native';
import App from './App';
import {name as appName} from './app.json';
//import 'react-native-gesture-handler';
import { GoogleSignin } from '@react-native-google-signin/google-signin';
//Merge de la rama integracion hacia master

GoogleSignin.configure({
    webClientId: '292495417842-mjn0cnfaenocjumj4419edbsjaha2f7o.apps.googleusercontent.com',
    //ios:'292495417842-u0hnt9l8jjfm1ahovj4iulsmfk0t1u2p.apps.googleusercontent.com'
});

AppRegistry.registerComponent(appName, () => App);
