import React, {useEffect } from 'react';
import {
  StyleSheet,
  StatusBar,
  SafeAreaView,
} from 'react-native';
import {
  Provider as PaperProvider,
  configureFonts,
  DefaultTheme as PaperDefaultTheme,
} from 'react-native-paper';
import { NavigationContainer } from '@react-navigation/native';
import { theme, fonts } from './src/res/theme';
import SplashScreen from 'react-native-splash-screen';
import RouterStack from './src/router/RouterStack';

const fontConfig = fonts;
function App() {


  PaperDefaultTheme.colors.primary = theme.primaryColor;
  PaperDefaultTheme.colors.text = theme.primaryTextColor;
  PaperDefaultTheme.colors.placeholder = theme.secondaryTextColor;
  PaperDefaultTheme.fonts = configureFonts(fontConfig);

  PaperDefaultTheme.colors.primary = theme.primaryColor;
  PaperDefaultTheme.colors.accent = theme.accentColor;
  PaperDefaultTheme.fonts = configureFonts(fontConfig);
  useEffect(() => {
    SplashScreen.hide();

  }, []);

    return (
    <SafeAreaView style={styles.safe}>
      <PaperProvider theme={PaperDefaultTheme}>
        <StatusBar backgroundColor={'black'}  />
        <NavigationContainer>
          <RouterStack />
        </NavigationContainer>
      </PaperProvider>
    </SafeAreaView>
  );
}

export default App;

const styles = StyleSheet.create({
  louder: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  backgorund: {
    flex: 1,
  },
  safe: { flex: 1 },
});
