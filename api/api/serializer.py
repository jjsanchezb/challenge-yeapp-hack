from api.models import Exercise, Information, User
from rest_framework import serializers


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = "__all__"


class ExerciseSerializer(serializers.ModelSerializer):
    class Meta:
        model = Exercise
        fields = "__all__"


class InformationSerializer(serializers.ModelSerializer):
    student = UserSerializer(read_only=True)
    studentId = serializers.PrimaryKeyRelatedField(queryset=User.objects.all(), source="student", write_only=True)
    exerciseId = serializers.PrimaryKeyRelatedField(queryset=Exercise.objects.all(), source="exercise", write_only=True)
    exercise = ExerciseSerializer(read_only=True)

    class Meta:
        model = Information
        fields = "__all__"
