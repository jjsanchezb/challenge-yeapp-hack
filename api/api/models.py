from email.policy import default
from django.conf import settings
from django.db import models
from django.contrib.auth.models import AbstractUser
from django.utils.translation import gettext_lazy as _


class User(AbstractUser):
    class Role(models.TextChoices):
        TEACHER = "TE", _("Teacher")
        STUDENT = "ST", _("Student")

    role = models.CharField(
        max_length=2,
        choices=Role.choices,
        default=Role.STUDENT,
    )

    pass


class Exercise(models.Model):
    is_active = models.BooleanField(default=False)
    name = models.CharField(max_length=100)


class Information(models.Model):
    student = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.PROTECT)
    exercise = models.OneToOneField(Exercise, on_delete=models.PROTECT)
    data = models.JSONField(null=True)
