from django.urls import path
from rest_framework.routers import DefaultRouter
from api.api import InformationViewSet, UserViewSet, ExerciseViewSet


router = DefaultRouter()
router.register(r"information", InformationViewSet, basename="information")
router.register(r"user", UserViewSet, basename="user")
router.register(r"exercise", ExerciseViewSet, basename="exercise")
api_urlpatterns = router.urls
