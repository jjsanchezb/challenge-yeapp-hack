from rest_framework import viewsets
from api.serializer import UserSerializer, InformationSerializer, ExerciseSerializer
from api.models import User, Information, Exercise


class InformationViewSet(viewsets.ModelViewSet):
    """
    A viewset for viewing and editing Information instances.
    """

    serializer_class = InformationSerializer
    queryset = Information.objects.all()


class UserViewSet(viewsets.ModelViewSet):
    """
    A viewset for viewing and editing user instances.
    """

    serializer_class = UserSerializer
    queryset = User.objects.all()


class ExerciseViewSet(viewsets.ModelViewSet):
    """
    A viewset for viewing and editing Exercise instances.
    """

    serializer_class = ExerciseSerializer
    queryset = Exercise.objects.all()
